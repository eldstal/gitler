#!/usr/bin/env python3

import argparse
import glob
import sys
import os
import time
import subprocess
from yaspin import yaspin

ROOTDIR = os.path.dirname(os.path.realpath(__file__))
CHKDIR  = os.path.join(ROOTDIR, "checks")

def run_check(target, check):
  global CHKDIR
  check_cmd = os.path.join(CHKDIR, check)

  process = subprocess.run([ check_cmd, target ],
                             stdout=subprocess.PIPE,
                             universal_newlines=True)

  return (process.returncode == 0, process.stdout)


parser = argparse.ArgumentParser(description='Run basic security checks on git website dumps')

parser.add_argument('-v', '--verbose', action="store_true",
                    help="Show verbose info on positive results")

parser.add_argument('-c', '--checks', type=str,
                    help="Comma-separated list of checks to perform")

parser.add_argument('-C', '--disable-checks', type=str,
                    help="Comma-separated list of checks to exclude")

parser.add_argument('-o', '--results', type=str,
                    help="File where vulnerable targets are appended, one per line")

parser.add_argument('target', type=str, nargs="*",
                    help="Website dump directory or tarball to check")

conf = parser.parse_args()

all_checks=[ os.path.basename(x) for x in glob.glob( os.path.join(CHKDIR, "*") ) ]
all_checks=[ x for x in all_checks if x[0] != "_" ]
print("All supported checks: {}".format(all_checks))

checks = []
if (conf.checks is not None):
  whitelist = conf.checks.split(",")
  for c in whitelist:
    if c in all_checks:
      checks += [ c ]
    else:
      sys.stderr.write("Warning! Requested check {} not supported!\n".format(c))
else:
  checks = all_checks

if (conf.disable_checks is not None):
  blacklist = conf.disable_checks.split(",")
  checks = [ c for c in checks if c not in blacklist ]

print("Enabled checks: {}".format(checks))

SPINNER=yaspin().green.bold

step=0
SPINNER.start()
for target in conf.target:

  step += 1
  progress = "{} / {}".format(step, len(conf.target))
  vulnerable=[]

  for check in checks:
    SPINNER.text = "Checking ({}) {} [ {} ] {}...".format(progress, target, ", ".join(vulnerable), check)

    hit,output = run_check(target, check)

    if hit:
      vulnerable += [ check ]
      if (conf.verbose):
        if (output[-1] == '\n'): output = output[:-1]
        SPINNER.write("")
        SPINNER.write(output)


  if (len(vulnerable) > 0):
    SPINNER.write("{} vulnerable to [{}]".format(target, ", ".join(vulnerable)))
    if (conf.results is not None):
      with open(conf.results, "a+") as f:
        f.write("{}\n".format(target))
SPINNER.stop()
