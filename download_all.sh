#!/bin/bash

ROOT=$(cd `dirname ${0}` && pwd)

function die {
  echo $*
  exit 1
}

function canonicalize {
  local DIR=$(cd "$(dirname $1)" && pwd)
  local NAME="${DIR}/$(basename $1)"
  echo "$NAME"
}

DOMAINS=${1}
[ -f "${DOMAINS}" ] || die "$0 <domains.txt> <dest-dir>"
DOMAINS=$(canonicalize "${DOMAINS}")

DESTDIR=${2}
[ -n "${DESTDIR}" ] || die "$0 <domains.txt> <dest-dir>"
DESTDIR=$(canonicalize "${DESTDIR}")

cd "${ROOT}"
[ -f "GitTools/Dumper/gitdumper.sh" ] || git clone "https://github.com/internetwache/GitTools.git"

mkdir -p "${DESTDIR}"

# Shuffle so that we can run in parallel without* race conditions
shuf ${DOMAINS} \
  | while read DOMAIN; do

    DESTNAME=$(echo "${DOMAIN}" \
           | sed -re 's/https?:\/\///' \
           | sed -re 's/[^a-zA-Z0-9.-]/_/g' \
           | sed -re 's/_+/_/g')

    DEST_UNPACKED="${DESTDIR}/${DESTNAME}"
    DEST_PACKED="${DESTDIR}/${DESTNAME}.tar.bz2"

    [ -d "${DEST_UNPACKED}" ] && continue
    [ -f "${DEST_PACKED}" ] && continue

    echo "${DEST_UNPACKED}"
    [ -n "${TMUX_PANE}" ] && tmux rename-window -t "${TMUX_PANE}" "${DESTNAME}"

    # Don't use up the last four gigabytes, fail gracefully
    FREE_SPACE=$(df --output=avail "${DESTDIR}" | tail -n 1)
    [ "${FREE_SPACE}" -lt 4194304 ] && die "Disk space exhausted."

    mkdir -p "${DEST_UNPACKED}"

    ${ROOT}/GitTools/Dumper/gitdumper.sh "${DOMAIN}"/.git/  "${DEST_UNPACKED}"

    ${ROOT}/archive_dump "${DEST_UNPACKED}" "${DEST_PACKED}" || die "Failed to archive ${DEST_UNPACKED}"


  done
