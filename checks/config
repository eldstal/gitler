#!/bin/bash
# A test should take a single target directory or tarball as argument.
# It should return 0 (success) if the target matches the test.
# It should return non-0 (failure) if the target does not match.
# On success, feel free to list interesting information to stdout, such as matching files.

source $(dirname "$0")/_util.sh

TARGET=$(load_target "$@")

QUIET=--quiet

RESULTFILE=$(mktemp)
echo 1 > ${RESULTFILE}

PATHS="conf|cfg|setup|vars|include|common|define|global|init|db|auth|priv|protect|pass|cred"
KEYWORDS="password|secret|private[a-zA-Z]|key|api|smtp|(pass[^a-zA-Z ])|pswd|pwd"

BLACKLIST="/wp-|/units/|/components/|/examples/|/tests/|/vendor/|/vendors/|/Zend/|/language/|/languages/|/classes/|/themes/|/css/"
BLACKLIST_EXTS="\.(html|phtml|php|asp|svg|png|jpg|bmp|pm|cfm|sh|md|js|cfc|sql|py|pl|cgi|css|scss|pdf)$"


list_target_files "${TARGET}" \
  | egrep -e "${PATHS}" \
  | egrep -v "${BLACKLIST}" \
  | egrep -v "${BLACKLIST_EXTS}" \
  | while read FILENAME; do

    dump_target_file "${TARGET}" "${FILENAME}" \
      | egrep ${QUIET} -i \
              -e "${KEYWORDS}"
    if [ "$?" -eq 0 ]; then
      echo 0 > "${RESULTFILE}"
      echo "${FILENAME} may contain secret configuration."
    fi

  done

RESULT=$(cat "${RESULTFILE}")
rm "${RESULTFILE}"
exit ${RESULT}
