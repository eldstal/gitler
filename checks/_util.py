import os
import sys
import tarfile

# Returns (target,None) if target is a directory
# Returns (target,tarfile) if target  is a tarball
# Exits hard if the target is invalid
def get_target():
  if (len(sys.argv) < 2): sys.exit(0)
  target = sys.argv[1]

  if os.path.isfile(target):
    try:
      tf = tarfile.open(target, mode='r')
      return target,tf
    except:
      sys.exit(0)

  elif os.path.isdir(target):
    return target,None

  else:
    sys.exit(0)
