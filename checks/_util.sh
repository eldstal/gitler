#
# Ways to deal with both directories and tarballs transparently
#

function die {
  echo $* >/dev/stderr
  exit 66
}

function load_target {
  local TARGET="${1}"
  [ -f "${TARGET}" -o -d "${TARGET}" ] || die "Unable to handle target ${TARGET}"
  echo "${TARGET}"
}

# List the files in the target, excluding .git/
function list_target_files {
  local TARGET="${1}"

  if [ -f "${TARGET}" ]; then
    tar --exclude=.git -tf "${TARGET}" | egrep -v '/$'
    return $?
  elif [ -d "${TARGET}" ]; then
    find "${TARGET}" -type f -and -not -wholename '*/.git/*'
    return $?
  fi

  return 1
}

# List everything in the target, including .git/ and empty directories
function list_target {
  local TARGET="${1}"

  if [ -f "${TARGET}" ]; then
    tar --exclude=.git -tf "${TARGET}"
    return $?
  elif [ -d "${TARGET}" ]; then
    find "${TARGET}"
    return $?
  fi

  return 1
}


# Provide something from list_target_files
function dump_target_file {
  local TARGET="${1}"
  local FPATH="${2}"

  if [ -f "${TARGET}" ]; then
    tar --to-stdout -xf "${TARGET}" "${FPATH}"
    return $?
  elif [ -d "${TARGET}" ]; then
    cat "${FPATH}"
    return $?
  fi
}

# Provide something from list_target_files
function target_file_size {
  local TARGET="${1}"
  local FPATH="${2}"

  if [ -f "${TARGET}" ]; then
    tar --to-stdout -xf "${TARGET}" "${FPATH}" | wc --bytes
    return $?
  elif [ -d "${TARGET}" ]; then
    du -bs "${FPATH}" | cut -f 1
    return $?
  fi
}
