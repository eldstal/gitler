import multiprocessing
import time
import sys
import os
import pydump

def downloadAllTheThings(inputs):
    (site, outdir) = inputs
    print(f"[s] {site}")
    gd = pydump.gitdump(site, outdir)
    gd.download()
    print(f"[d] {site}")

if __name__ == '__main__':
    if len(sys.argv) < 3:
        print("Usage: python3 pooldump.py file-with-domains outdir")

    with open(sys.argv[1], "r") as f:
        dat = [ (os.path.join("http://",x.strip(), ".git/"), os.path.join(sys.argv[2], x.strip(), ".git/")) for x in f.readlines()]
    p = multiprocessing.Pool(50)
    p.map(downloadAllTheThings, dat)
